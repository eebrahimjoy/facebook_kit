package com.example.facebook_kit;

import android.app.Fragment;
import android.os.Parcel;
import android.support.annotation.Nullable;

import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.ui.BaseUIManager;
import com.facebook.accountkit.ui.ButtonType;
import com.facebook.accountkit.ui.LoginFlowState;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.TextPosition;

public class FacebookKitUIManager extends BaseUIManager {
    private static final int HEADER_HEIGHT = 90;

    private final ButtonType confirmButton;
    private final ButtonType entryButton;
    private AccountKitError error;
    private LoginType loginType;
    private final TextPosition textPosition;

    @Deprecated
    public FacebookKitUIManager(
            final ButtonType confirmButton,
            final ButtonType entryButton,
            final TextPosition textPosition,
            final LoginType loginType) {
        super(-1);
        this.confirmButton = confirmButton;
        this.entryButton = entryButton;
        this.textPosition = textPosition;
        this.loginType = loginType;
    }

    private FacebookKitUIManager(final Parcel source) {
        super(source);
        this.loginType = LoginType.values()[source.readInt()];
        String s = source.readString();
        final ButtonType confirmButton = s == null ? null : ButtonType.valueOf(s);
        s = source.readString();
        final ButtonType entryButton = s == null ? null : ButtonType.valueOf(s);
        s = source.readString();
        final TextPosition textPosition = s == null ? null : TextPosition.valueOf(s);
        this.confirmButton = confirmButton;
        this.entryButton = entryButton;
        this.textPosition = textPosition;
    }


    @Override
    @Nullable
    public ButtonType getButtonType(final LoginFlowState state) {
        switch (state) {
            case PHONE_NUMBER_INPUT:
            case EMAIL_INPUT:
                return entryButton;
            case CODE_INPUT:
            case CONFIRM_ACCOUNT_VERIFIED:
                return confirmButton;
            default:
                return null;
        }
    }


    @Override
    @Nullable
    public Fragment getHeaderFragment(final LoginFlowState state) {
        if (state != LoginFlowState.ERROR) {
            return getPlaceholderFragment(state, HEADER_HEIGHT, "");
        }
        final String errorMessage = getErrorMessage();
        if (errorMessage == null) {
            return FacebookKitPlaceholderFragment.create(HEADER_HEIGHT, R.string.error_message);
        } else {
            return FacebookKitPlaceholderFragment.create(HEADER_HEIGHT, errorMessage);
        }
    }

    @Override
    @Nullable
    public TextPosition getTextPosition(final LoginFlowState state) {
        return textPosition;
    }

    @Override
    public void onError(final AccountKitError error) {
        this.error = error;
    }

    private String getErrorMessage() {
        if (error == null) {
            return null;
        }

        final String message = error.getUserFacingMessage();
        if (message == null) {
            return null;
        }

        return message;
    }

    @Nullable
    private FacebookKitPlaceholderFragment getPlaceholderFragment(
            final LoginFlowState state,
            final int height,
            final String suffix) {
        final String prefix;
        switch (state) {
            case PHONE_NUMBER_INPUT:
                prefix = "Enter Customer Phone Number(Editable)";
                break;
            case ACCOUNT_VERIFIED:
                prefix = "Verified (Editable)";
                break;
            case CONFIRM_ACCOUNT_VERIFIED:
                prefix = "Verified (Editable) ";
                break;
            case CONFIRM_INSTANT_VERIFICATION_LOGIN:
                prefix = "Confirm Instant Verification Login (Editable) ";
                break;
            case SENDING_CODE:
                switch (loginType) {
                    case PHONE:
                        prefix = "Sending Code (Editable) ";
                        break;
                    default:
                        return null;
                }
                break;
            case SENT_CODE:
                switch (loginType) {
                    case PHONE:
                        prefix = "Sent Code (Editable) ";
                        break;
                    default:
                        return null;
                }
                break;
            case CODE_INPUT:
                prefix = "Code Input (Editable) ";
                break;
            case VERIFYING_CODE:
                prefix = "Verifying (Editable) ";
                break;
            case VERIFIED:
                prefix = "Verified (Editable) ";
                break;
            case RESEND:
                prefix = "Resend (Editable) ";
                break;
            case ERROR:
                prefix = "Error (Editable) ";
                break;
            default:
                return null;
        }
        return FacebookKitPlaceholderFragment.create(height, prefix.concat(suffix));
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(loginType.ordinal());
        dest.writeString(confirmButton != null ? confirmButton.name() : null);
        dest.writeString(entryButton != null ? entryButton.name() : null);
        dest.writeString(textPosition != null ? textPosition.name() : null);
    }

    public static final Creator<FacebookKitUIManager> CREATOR
            = new Creator<FacebookKitUIManager>() {
        @Override
        public FacebookKitUIManager createFromParcel(final Parcel source) {
            return new FacebookKitUIManager(source);
        }

        @Override
        public FacebookKitUIManager[] newArray(final int size) {
            return new FacebookKitUIManager[size];
        }
    };
}
