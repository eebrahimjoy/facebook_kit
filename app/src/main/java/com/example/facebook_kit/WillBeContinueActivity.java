package com.example.facebook_kit;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.facebook_kit.databinding.ActivityWillBeContinueBinding;

public class WillBeContinueActivity extends AppCompatActivity {

    ActivityWillBeContinueBinding binding;

    String phoneNumber;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_will_be_continue);
        phoneNumber =getIntent().getStringExtra("PHONE");
        binding.phone.setText(phoneNumber);
    }
}
